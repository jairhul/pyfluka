# Generated from FlukaParser.g4 by ANTLR 4.7
from antlr4 import *

# This class defines a complete listener for a parse tree produced by FlukaParser.
class FlukaParserListener(ParseTreeListener):

    # Enter a parse tree produced by FlukaParser#geocards.
    def enterGeocards(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#geocards.
    def exitGeocards(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#GeometryDirective.
    def enterGeometryDirective(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#GeometryDirective.
    def exitGeometryDirective(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#BodyDefSpaceDelim.
    def enterBodyDefSpaceDelim(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#BodyDefSpaceDelim.
    def exitBodyDefSpaceDelim(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#BodyDefPunctDelim.
    def enterBodyDefPunctDelim(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#BodyDefPunctDelim.
    def exitBodyDefPunctDelim(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#simpleRegion.
    def enterSimpleRegion(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#simpleRegion.
    def exitSimpleRegion(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#complexRegion.
    def enterComplexRegion(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#complexRegion.
    def exitComplexRegion(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#multipleUnion.
    def enterMultipleUnion(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#multipleUnion.
    def exitMultipleUnion(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#singleUnion.
    def enterSingleUnion(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#singleUnion.
    def exitSingleUnion(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#multipleUnion2.
    def enterMultipleUnion2(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#multipleUnion2.
    def exitMultipleUnion2(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#zone.
    def enterZone(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#zone.
    def exitZone(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#singleUnary.
    def enterSingleUnary(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#singleUnary.
    def exitSingleUnary(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#unaryAndBoolean.
    def enterUnaryAndBoolean(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#unaryAndBoolean.
    def exitUnaryAndBoolean(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#unaryAndSubZone.
    def enterUnaryAndSubZone(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#unaryAndSubZone.
    def exitUnaryAndSubZone(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#oneSubZone.
    def enterOneSubZone(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#oneSubZone.
    def exitOneSubZone(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#subZone.
    def enterSubZone(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#subZone.
    def exitSubZone(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#unaryExpression.
    def enterUnaryExpression(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#unaryExpression.
    def exitUnaryExpression(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#geoDirective.
    def enterGeoDirective(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#geoDirective.
    def exitGeoDirective(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#expansion.
    def enterExpansion(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#expansion.
    def exitExpansion(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#translat.
    def enterTranslat(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#translat.
    def exitTranslat(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#transform.
    def enterTransform(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#transform.
    def exitTransform(self, ctx):
        pass


    # Enter a parse tree produced by FlukaParser#lattice.
    def enterLattice(self, ctx):
        pass

    # Exit a parse tree produced by FlukaParser#lattice.
    def exitLattice(self, ctx):
        pass


