install:
	pip install . --user

uninstall:
	pip uninstall pyfluka

develop:
	pip install --editable . --user
